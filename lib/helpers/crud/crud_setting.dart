import 'package:frflutter/helpers/collections/db_settings_model.dart';
import 'package:frflutter/helpers/crud/crud.dart';
import 'package:frflutter/models/settings.dart';

class CRUDSetting extends CRUD {
  CRUDSetting._privateConstructor();
  final _model = DBSettingsModel();
  static final CRUDSetting init = CRUDSetting._privateConstructor();

  @override
  get model => _model;

  Future<List<Settings>> read({String? where, List? whereArgs}) async =>
      (await super.fetch()).map((e) => Settings.fromMap(e)).toList();

  Future<int> upsert(Settings data) async {
    if (data.id != null) return await super.update(data.toMap());
    return await super.insert(data.toMap());
  }
}
