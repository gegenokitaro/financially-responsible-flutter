import 'package:frflutter/helpers/collections/db_wallets_model.dart';
import 'package:frflutter/helpers/crud/crud.dart';
import 'package:frflutter/models/wallet.dart';

class CRUDWallet extends CRUD {
  CRUDWallet._privateConstructor();
  final _model = DBWalletsModel();
  static final CRUDWallet init = CRUDWallet._privateConstructor();

  @override
  get model => _model;

  Future<List<Wallet>> read({String? where, List? whereArgs}) async =>
      (await super.fetch()).map((e) => Wallet.fromMap(e)).toList();

  Future<int> upsert(Wallet data) async {
    if (data.id != null) return await super.update(data.toMap());
    return await super.insert(data.toMap());
  }
}
