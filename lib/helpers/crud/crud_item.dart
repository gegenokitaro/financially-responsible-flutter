import 'package:frflutter/helpers/collections/db_items_model.dart';
import 'package:frflutter/helpers/crud/crud.dart';
import 'package:frflutter/models/activity_item.dart';

class CRUDItem extends CRUD {
  CRUDItem._privateConstructor();
  final _model = DBItemsModel();
  static final CRUDItem init = CRUDItem._privateConstructor();

  @override
  get model => _model;

  Future<List<ActivityItem>> read({String? where, List? whereArgs}) async {
    String sql = '''
        SELECT
          ${_model.tableName}.id
          ,  ${_model.tableName}.isIncome
          , ${_model.tableName}.categoryID
          , categories.name as categoryName
          , ${_model.tableName}.date
          , ${_model.tableName}.price
          , ${_model.tableName}.description
          , ${_model.tableName}.description
          , categories.delegateCategoryID
          , ${_model.tableName}.delegateFrom
        FROM ${_model.tableName}
        LEFT JOIN categories
          ON categories.id = ${_model.tableName}.categoryID
        WHERE 1=?
        ${where != null ? 'AND $where' : ''}
        ORDER BY date
      ''';

    whereArgs != null ? [1, ...whereArgs] : [1];
    return (await super.rawQuery(sql, whereArgs: whereArgs))
        .map((e) => ActivityItem.fromMap(e))
        .toList();
  }

  Future<int> upsert(ActivityItem data) async {
    if (data.id != null) return await super.update(data.toMap());
    return await super.insert(data.toMap());
  }
}
