import 'package:frflutter/helpers/dbhelper.dart';

class CRUD {
  final model = null;

  Future<List<dynamic>> rawQuery(String sql, {List? whereArgs}) async {
    var result = await DBHelper.instance.rawQuery(sql, whereArgs: whereArgs);
    return result;
  }

  Future<List<dynamic>> fetch({String? where, List? whereArgs}) async {
    var result = await DBHelper.instance.fetch(
      model.tableName,
      where: where,
      whereArgs: whereArgs,
    );
    return result;
  }

  Future<int> insert(Map<String, dynamic> data) async {
    var result = await DBHelper.instance.insert(model.tableName, data);
    return result;
  }

  Future<int> update(Map<String, dynamic> data) async {
    var result = await DBHelper.instance.update(model.tableName, data);
    return result;
  }

  Future<int> delete(int id) async {
    var result = await DBHelper.instance.delete(model.tableName, id);
    return result;
  }
}
