import 'package:frflutter/helpers/collections/db_wishlist_group_model.dart';
import 'package:frflutter/helpers/crud/crud.dart';
import 'package:frflutter/models/wishlist_group.dart';

class CRUDWishlistGroup extends CRUD {
  CRUDWishlistGroup._privateConstructor();
  final _model = DBWishlistGroupModel();
  static final CRUDWishlistGroup init = CRUDWishlistGroup._privateConstructor();

  @override
  get model => _model;

  Future<List<WishlistGroup>> read({String? where, List? whereArgs}) async =>
      (await super.fetch()).map((e) => WishlistGroup.fromMap(e)).toList();

  Future<int> upsert(WishlistGroup data) async {
    if (data.id != null) return await super.update(data.toMap());
    return await super.insert(data.toMap());
  }
}
