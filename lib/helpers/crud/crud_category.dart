import 'package:frflutter/helpers/collections/db_categories_model.dart';
import 'package:frflutter/helpers/crud/crud.dart';
import 'package:frflutter/models/activity_category.dart';

class CRUDCategory extends CRUD {
  CRUDCategory._privateConstructor();
  final _model = DBCategoriesModel();
  static final CRUDCategory init = CRUDCategory._privateConstructor();

  @override
  get model => _model;

  Future<List<ActivityCategory>> read({String? where, List? whereArgs}) async =>
      (await super.fetch()).map((e) => ActivityCategory.fromMap(e)).toList();

  Future<int> upsert(ActivityCategory data) async {
    if (data.id != null) return await super.update(data.toMap());
    return await super.insert(data.toMap());
  }
}
