import 'package:frflutter/helpers/collections/db_wishlist_income_model.dart';
import 'package:frflutter/helpers/crud/crud.dart';
import 'package:frflutter/models/wishlist_income.dart';

class CRUDWishlistIncome extends CRUD {
  CRUDWishlistIncome._privateConstructor();
  final _model = DBWishlistIncomeModel();
  static final CRUDWishlistIncome init =
      CRUDWishlistIncome._privateConstructor();

  @override
  get model => _model;

  Future<List<WishlistIncome>> read({String? where, List? whereArgs}) async =>
      (await super.fetch()).map((e) => WishlistIncome.fromMap(e)).toList();

  Future<int> upsert(WishlistIncome data) async {
    if (data.id != null) return await super.update(data.toMap());
    return await super.insert(data.toMap());
  }
}
