import 'package:frflutter/helpers/collections/db_wishlist_model.dart';
import 'package:frflutter/helpers/crud/crud.dart';
import 'package:frflutter/models/wishlist.dart';

class CRUDWishlist extends CRUD {
  CRUDWishlist._privateConstructor();
  final _model = DBWishlistModel();
  static final CRUDWishlist init = CRUDWishlist._privateConstructor();

  @override
  get model => _model;

  Future<List<Wishlist>> read({String? where, List? whereArgs}) async =>
      (await super.fetch()).map((e) => Wishlist.fromMap(e)).toList();

  Future<int> upsert(Wishlist data) async {
    if (data.id != null) return await super.update(data.toMap());
    return await super.insert(data.toMap());
  }
}
