import 'package:frflutter/helpers/collections/db_items_template_model.dart';
import 'package:frflutter/helpers/crud/crud.dart';
import 'package:frflutter/models/activity_item_template.dart';

class CRUDItemTemplate extends CRUD {
  CRUDItemTemplate._privateConstructor();
  final _model = DBItemsTemplateModel();
  static final CRUDItemTemplate init = CRUDItemTemplate._privateConstructor();

  @override
  get model => _model;

  Future<List<ActivityItemTemplate>> read(
          {String? where, List? whereArgs}) async =>
      (await super.fetch())
          .map((e) => ActivityItemTemplate.fromMap(e))
          .toList();

  Future<int> upsert(ActivityItemTemplate data) async {
    if (data.id != null) return await super.update(data.toMap());
    return await super.insert(data.toMap());
  }
}
