import 'dart:io';

import 'package:frflutter/constants.dart';
import 'package:frflutter/helpers/collections/db_categories_model.dart';
import 'package:frflutter/helpers/collections/db_items_model.dart';
import 'package:frflutter/helpers/collections/db_items_template_model.dart';
import 'package:frflutter/helpers/collections/db_settings_model.dart';
import 'package:frflutter/helpers/collections/db_wallets_model.dart';
import 'package:frflutter/helpers/collections/db_wishlist_group_model.dart';
import 'package:frflutter/helpers/collections/db_wishlist_income_model.dart';
import 'package:frflutter/helpers/collections/db_wishlist_model.dart';
import 'package:frflutter/models/activity_category.dart';
import 'package:frflutter/models/wallet.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  DBHelper._privateConstructor();
  static final DBHelper instance = DBHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    var files = Directory(documentsDirectory.path).listSync();
    for (var element in files) {
      databaseFactory.deleteDatabase(element.path);
    }
    String path = join(documentsDirectory.path, SQLITE_DBNAME);
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  final _mWallets = DBWalletsModel();
  final _mCategories = DBCategoriesModel();
  final _mItems = DBItemsModel();
  final _mItemsTemplate = DBItemsTemplateModel();
  final _mWishlistGroup = DBWishlistGroupModel();
  final _mWishlistIncome = DBWishlistIncomeModel();
  final _mWishlist = DBWishlistModel();
  final _mSettings = DBSettingsModel();

  Future _onCreate(Database db, int version) async {
    try {
      await db.execute(_mWallets.dbTable());
      await db.execute(_mCategories.dbTable());
      await db.execute(_mItems.dbTable());
      await db.execute(_mItemsTemplate.dbTable());
      await db.execute(_mWishlistGroup.dbTable());
      await db.execute(_mWishlistIncome.dbTable());
      await db.execute(_mWishlist.dbTable());
      await db.execute(_mSettings.dbTable());
    } catch (e) {
      rethrow;
    }

    await _defaultData(db);
  }

  Future _defaultData(Database db) async {
    try {
      await Future.forEach(_mWallets.defaultData,
          (e) => db.insert(_mWallets.tableName, e.toMap()));
      await Future.forEach(_mCategories.defaultData,
          (e) => db.insert(_mCategories.tableName, e.toMap()));
      await Future.forEach(_mSettings.defaultData,
          (e) => db.insert(_mSettings.tableName, e.toMap()));
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Map<String, dynamic>>> rawQuery(String sql,
      {List? whereArgs}) async {
    whereArgs = whereArgs ?? [];
    Database db = await instance.database;
    var result = await db.rawQuery(sql, whereArgs);
    return result;
  }

  Future<List<Map<String, dynamic>>> fetch(String table,
      {String? where, List? whereArgs}) async {
    where = where ?? '1=?';
    whereArgs = whereArgs ?? [1];
    Database db = await instance.database;
    var result = await db.query(table, where: where, whereArgs: whereArgs);
    return result;
  }

  Future<int> insert(String table, Map<String, dynamic> data) async {
    Database db = await instance.database;
    var result = await db.insert(table, data);
    return result;
  }

  Future<int> update(String table, Map<String, dynamic> data) async {
    Database db = await instance.database;
    var result = await db.update(
      table,
      data,
      where: 'id = ?',
      whereArgs: [data['id']],
    );
    return result;
  }

  Future<int> delete(String table, int id) async {
    Database db = await instance.database;
    var result = await db.delete(
      table,
      where: 'id = ?',
      whereArgs: [id],
    );
    return result;
  }

  Future<List<Wallet>> getWallet() async {
    try {
      Database db = await instance.database;
      var result = await db.query('wallets');
      List<Wallet> listWallet = result.map((e) => Wallet.fromMap(e)).toList();
      return listWallet;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<ActivityCategory>> getCategory() async {
    try {
      Database db = await instance.database;
      var result = await db.query(_mCategories.tableName);
      List<ActivityCategory> listCategory =
          result.map((e) => ActivityCategory.fromMap(e)).toList();
      return listCategory;
    } catch (e) {
      rethrow;
    }
  }

  Future closeDB() async {
    Database db = await instance.database;
    _database = null;
    await db.close();
  }
}
