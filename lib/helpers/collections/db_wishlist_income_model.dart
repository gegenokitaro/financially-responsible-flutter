import 'package:frflutter/helpers/table_model.dart';

class DBWishlistIncomeModel {
  DBWishlistIncomeModel();

  final String _tableName = 'wishlist_income';

  String get tableName => _tableName;

  String dbTable() {
    return TableModel(tableName, <TableColumnModel>[
      TableColumnModel(name: 'wishlistGroupID', type: TableType.integer),
      TableColumnModel(name: 'categoryID', type: TableType.integer),
    ]).toSQL();
  }
}
