import 'package:frflutter/helpers/table_model.dart';

class DBItemsTemplateModel {
  DBItemsTemplateModel();

  final String _tableName = 'items_template';

  String get tableName => _tableName;

  String dbTable() {
    return TableModel(tableName, <TableColumnModel>[
      TableColumnModel(
          name: 'isIncome',
          type: TableType.integer,
          nullable: false,
          defaultValue: '0'),
      TableColumnModel(
          name: 'categoryID', type: TableType.integer, nullable: false),
      TableColumnModel(name: 'description', type: TableType.text)
    ]).toSQL();
  }
}
