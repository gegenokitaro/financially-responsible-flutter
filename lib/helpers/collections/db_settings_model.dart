import 'package:frflutter/helpers/table_model.dart';
import 'package:frflutter/models/settings.dart';

class DBSettingsModel {
  DBSettingsModel();

  final String _tableName = 'settings';

  String get tableName => _tableName;

  String dbTable() {
    return TableModel(tableName, <TableColumnModel>[
      TableColumnModel(
          name: 'currentWalletID',
          type: TableType.integer,
          nullable: false,
          defaultValue: '0'),
    ]).toSQL();
  }

  List<Settings> defaultData = [
    Settings(currentWalletID: 1),
  ];
}
