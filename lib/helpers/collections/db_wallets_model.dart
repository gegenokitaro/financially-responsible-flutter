import 'package:frflutter/helpers/table_model.dart';
import 'package:frflutter/models/wallet.dart';

class DBWalletsModel {
  DBWalletsModel();

  final String _tableName = 'wallets';

  String get tableName => _tableName;

  String dbTable() {
    return TableModel(tableName, <TableColumnModel>[
      TableColumnModel(name: 'name', type: TableType.text, nullable: false),
    ]).toSQL();
  }

  List<Wallet> defaultData = [
    Wallet(name: 'Default'),
    Wallet(name: 'Tabungan'),
  ];
}
