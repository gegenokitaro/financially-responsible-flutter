import 'package:frflutter/helpers/table_model.dart';
import 'package:frflutter/models/activity_category.dart';

class DBCategoriesModel {
  DBCategoriesModel();

  final String _tableName = 'categories';

  String get tableName => _tableName;

  String dbTable() {
    return TableModel(tableName, <TableColumnModel>[
      TableColumnModel(name: 'name', type: TableType.text, nullable: false),
      TableColumnModel(
          name: 'isIncome',
          type: TableType.integer,
          nullable: false,
          defaultValue: '0'),
      TableColumnModel(
          name: 'walletID', type: TableType.integer, nullable: false),
      TableColumnModel(name: 'delegateCategoryID', type: TableType.integer),
    ]).toSQL();
  }

  List<ActivityCategory> defaultData = [
    ActivityCategory(
      isIncome: false,
      name: 'Belanja',
      walletID: 1,
    ),
    ActivityCategory(
      isIncome: false,
      name: 'Tagihan',
      walletID: 1,
    ),
    ActivityCategory(
      isIncome: false,
      name: 'Lain-lain',
      walletID: 1,
    ),
    ActivityCategory(
      isIncome: true,
      name: 'Gaji',
      walletID: 1,
    ),
  ];
}
