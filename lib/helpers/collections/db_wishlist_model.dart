import 'package:frflutter/helpers/table_model.dart';

class DBWishlistModel {
  DBWishlistModel();
  final String _tableName = 'wishlist';

  String get tableName => _tableName;

  String dbTable() {
    return TableModel(tableName, <TableColumnModel>[
      TableColumnModel(name: 'name', type: TableType.text),
      TableColumnModel(name: 'price', type: TableType.real),
      TableColumnModel(name: 'priority', type: TableType.integer),
      TableColumnModel(
          name: 'wishlistGroupId', type: TableType.integer, defaultValue: '0'),
    ]).toSQL();
  }
}
