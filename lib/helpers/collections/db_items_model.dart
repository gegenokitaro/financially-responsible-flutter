import 'package:frflutter/helpers/table_model.dart';

class DBItemsModel {
  DBItemsModel();

  final String _tableName = 'items';

  String get tableName => _tableName;

  String dbTable() {
    return TableModel(tableName, <TableColumnModel>[
      TableColumnModel(
          name: 'isIncome',
          type: TableType.integer,
          nullable: false,
          defaultValue: '0'),
      TableColumnModel(
          name: 'date',
          type: TableType.datetime,
          defaultValue: 'CURRENT_TIMESTAMP'),
      TableColumnModel(
          name: 'categoryID', type: TableType.integer, nullable: false),
      TableColumnModel(name: 'price', type: TableType.real),
      TableColumnModel(name: 'description', type: TableType.text),
      TableColumnModel(name: 'delegateFrom', type: TableType.integer),
    ]).toSQL();
  }
}
