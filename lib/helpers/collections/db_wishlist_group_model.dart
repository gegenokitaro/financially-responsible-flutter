import 'package:frflutter/helpers/table_model.dart';

class DBWishlistGroupModel {
  DBWishlistGroupModel();

  final String _tableName = 'wishlist_group';

  String get tableName => _tableName;

  String dbTable() {
    return TableModel(tableName, <TableColumnModel>[
      TableColumnModel(name: 'name', type: TableType.text),
      TableColumnModel(name: 'substractFrom', type: TableType.integer),
      TableColumnModel(name: 'substractCategoryFrom', type: TableType.integer),
      TableColumnModel(name: 'priority', type: TableType.integer),
    ]).toSQL();
  }
}
