class TableModel {
  final String name;
  final List<TableColumnModel> _columns;

  List<TableColumnModel> get columnInput => [];

  TableModel(this.name, List<TableColumnModel> columnInput)
      : _columns = [
          TableColumnModel(
            name: 'id',
            type: TableType.integer,
            autoIncrement: true,
          ),
          TableColumnModel(
              name: 'createdAt',
              type: TableType.datetime,
              defaultValue: 'CURRENT_TIMESTAMP'),
          TableColumnModel(
              name: 'updatedAt',
              type: TableType.datetime,
              defaultValue: 'CURRENT_TIMESTAMP'),
          ...columnInput
        ];

  String toSQL() {
    String tableName = name;
    String columns = _columns.map((e) {
      var name = e.name;
      var type = ' ${typeValue(e.type)}';
      var autoIncrement = e.autoIncrement != null
          ? e.autoIncrement!
              ? ' AUTO INCREMENT'
              : ''
          : '';
      var nullable = e.nullable != null
          ? e.nullable!
              ? ''
              : ' NOT NULL'
          : '';

      var defaultValue =
          e.defaultValue != null ? ' DEFAULT ${e.defaultValue}' : '';
      return "$name$type$autoIncrement$nullable$defaultValue";
    }).join(', ');
    return "CREATE TABLE IF NOT EXISTS $tableName ($columns);";
  }
}

class TableColumnModel {
  final String name;
  final TableType type;
  final bool? primaryKey;
  final bool? autoIncrement;
  final bool? nullable;
  final String? defaultValue;

  TableColumnModel(
      {required this.name,
      required this.type,
      this.primaryKey,
      this.autoIncrement,
      this.nullable,
      this.defaultValue});
}

enum TableType { integer, text, datetime, real }

String typeValue(type) {
  switch (type) {
    case TableType.integer:
      return 'INTEGER';
    case TableType.text:
      return 'TEXT';
    case TableType.datetime:
      return 'DATETIME';
    case TableType.real:
      return 'REAL';
    default:
      return '';
  }
}
