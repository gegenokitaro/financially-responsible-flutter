import 'package:flutter/material.dart';
import 'package:frflutter/helpers/dbhelper.dart';
import 'package:frflutter/modals/item_modal.dart';
import 'package:frflutter/pages/settings_page.dart';
import 'package:frflutter/pages/summary_page.dart';
import 'package:frflutter/pages/wallet_page.dart';
import 'package:frflutter/pages/wishlist_page.dart';
import 'package:frflutter/providers/activity_items_provider.dart';
import 'package:frflutter/providers/current_wallet_provider.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const HomePage());
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void dispose() {
    // TODO: implement dispose
    DBHelper.instance.closeDB();
    super.dispose();
  }

  List bottomMenu = <Map>[
    {
      "icon": Icons.account_balance_wallet_outlined,
      "widget": const WalletPage(),
    },
    {
      "icon": Icons.receipt_long_outlined,
      "widget": const SummaryPage(),
    },
    {
      "icon": Icons.auto_awesome_outlined,
      "widget": const WishlistPage(),
    },
    {
      "icon": Icons.settings_outlined,
      "widget": const SettingsPage(),
    }
  ];

  int currentIndex = 0;

  void goToPage(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    print('main state');
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ActivityItemsProvider()),
        ChangeNotifierProvider(create: (context) => CurrentWalletProvider()),
      ],
      child: MaterialApp(
          home: Scaffold(
              body: bottomMenu[currentIndex]['widget'],
              floatingActionButton: Visibility(
                  visible: currentIndex != 3, child: const NewItem()),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.endDocked,
              bottomNavigationBar: BottomAppBar(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: List.generate(
                        bottomMenu.length,
                        (index) => BottomBarIcon(
                              icon: bottomMenu[index]["icon"],
                              index: index,
                              onTap: goToPage,
                              isSelected: currentIndex == index ? true : false,
                            )),
                  ),
                ),
              ))),
    );
  }
}

class NewItem extends StatelessWidget {
  const NewItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        onPressed: () => showModalBottomSheet(
            isScrollControlled: true,
            // shape:
            // RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            context: context,
            builder: (context) => const ItemModal()),
        child: const Icon(Icons.post_add_outlined));
  }
}

// ignore: must_be_immutable
class BottomBarIcon extends StatelessWidget {
  BottomBarIcon(
      {super.key,
      required this.icon,
      required this.index,
      required this.onTap,
      this.isSelected = false});

  final IconData icon;
  final int index;
  final Function onTap;
  bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: Container(
        decoration: BoxDecoration(
            border: isSelected
                ? const Border(bottom: BorderSide(width: 5, color: Colors.blue))
                : null),
        child: IconButton(
          onPressed: () => onTap(index),
          icon: Icon(icon),
          iconSize: 32,
        ),
      ),
    );
  }
}
