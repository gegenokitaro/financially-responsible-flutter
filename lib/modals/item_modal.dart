import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../widgets/list_category.dart';
import '../widgets/item_button.dart';
import '../widgets/item_input_clickable.dart';
import '../widgets/item_input_field.dart';

class ItemModal extends StatefulWidget {
  const ItemModal({super.key});

  @override
  State<ItemModal> createState() => _ItemModalState();
}

class _ItemModalState extends State<ItemModal> {
  int _isIncome = 0;
  final _priceController = TextEditingController();
  final _descriptionController = TextEditingController();
  DateTime _date = DateTime.now();
  String? _dateString;

  @override
  void dispose() {
    // TODO: implement dispose
    _priceController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  void _showModal(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) => ListCategory(
              isIncome: _isIncome == 1,
            ));
  }

  Future<void> _showDatePicker(BuildContext context) async {
    DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: _dateString == null ? DateTime.now() : _date,
        firstDate: DateTime(1970),
        lastDate: DateTime(DateTime.now().year + 2));

    if (pickedDate == null) return;

    setState(() {
      _date = pickedDate;
      _dateString = pickedDate.toIso8601String().split("T")[0];
    });
  }

  void _onCancel(BuildContext context) {
    Navigator.pop(context);
  }

  void _onSubmit(BuildContext context) {
    // _onCancel(context);
  }

  @override
  Widget build(BuildContext context) {
    print('item modal');
    return Container(
      padding: const EdgeInsets.only(top: 30),
      height: MediaQuery.of(context).size.height * 0.9,
      color: Colors.white,
      child: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Column(
          children: [
            Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(
                  right: 30,
                  left: 30,
                  bottom: 20,
                ),
                child: const Text(
                  'Tambah Aktivitas',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                )),
            Expanded(
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: CupertinoSlidingSegmentedControl(
                            groupValue: _isIncome,
                            children: const {
                              0: SelectType(
                                name: "Pengeluaran",
                              ),
                              1: SelectType(name: "Pemasukan")
                            },
                            onValueChanged: (int? i) => {
                                  if (i != null)
                                    {
                                      setState(() {
                                        _isIncome = i;
                                      })
                                    }
                                }),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ItemInputClickable(
                        iconLogo: Icons.assignment_outlined,
                        placeholder: 'Kategori',
                        onTap: _showModal,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ItemInputClickable(
                        iconLogo: Icons.date_range_outlined,
                        placeholder: 'Tanggal',
                        value: _dateString,
                        onTap: _showDatePicker,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ItemInputField(
                        maxLines: 1,
                        placeholder: 'Nominal',
                        icon: Icons.price_change_outlined,
                        inputType: TextInputType.number,
                        inputController: _priceController,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ItemInputField(
                        placeholder: 'Deskripsi',
                        maxLines: 3,
                        icon: Icons.description_outlined,
                        inputController: _descriptionController,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          ModalButton(
                            backgroundColor: Colors.white,
                            foregroundColor: Colors.blue.shade800,
                            text: "Cancel",
                            onPressed: (context) => _onCancel(context),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          ModalButton(
                              backgroundColor: Colors.blue.shade800,
                              foregroundColor: Colors.white,
                              text: "Submit",
                              onPressed: (context) => _onSubmit(context))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SelectType extends StatelessWidget {
  const SelectType({super.key, required this.name});

  final String name;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      child: Text(name),
    );
  }
}
