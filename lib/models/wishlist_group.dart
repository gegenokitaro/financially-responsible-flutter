class WishlistGroup {
  final int? id;
  final String name;

  WishlistGroup({
    this.id,
    required this.name,
  });

  factory WishlistGroup.fromMap(Map<String, dynamic> json) => WishlistGroup(
        id: json['id'],
        name: json['name'],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) map['id'] = id;
    map['name'] = name;
    return map;
  }
}
