class Settings {
  int? id;
  int currentWalletID;

  Settings({this.id, required this.currentWalletID});

  factory Settings.fromMap(Map<String, dynamic> json) => Settings(
        id: json['id'],
        currentWalletID: int.parse(json['currentWalletID']),
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) map['id'] = id;
    map['currentWalletID'] = currentWalletID;
    return map;
  }
}
