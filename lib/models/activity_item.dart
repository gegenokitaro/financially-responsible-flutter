class ActivityItem {
  final int? id;
  final bool isIncome;
  final int categoryID;
  final String categoryName;
  final DateTime date;
  final double price;
  final String? description;
  final int? delegateCategoryID;
  final int? delegateFrom;

  ActivityItem({
    this.id,
    required this.isIncome,
    required this.categoryID,
    required this.categoryName,
    required this.date,
    required this.price,
    this.description,
    this.delegateCategoryID,
    this.delegateFrom,
  });

  factory ActivityItem.fromMap(Map<String, dynamic> json) => ActivityItem(
        isIncome: json['isIncome'] == 1 ? true : false,
        categoryID: json['categoryID'],
        categoryName: json['categoryName'],
        date: DateTime.parse(json['date']),
        price: double.parse(json['price']),
        description: json['description'],
        delegateCategoryID: json['delegateCategoryID'],
        delegateFrom: json['delegateFrom'],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) map['id'] = id;
    map['isIncome'] = isIncome ? 1 : 0;
    map['categoryID'] = categoryID;
    map['date'] = date.toIso8601String();
    map['price'] = price;
    if (description != null) map['description'] = description;
    return map;
  }
}
