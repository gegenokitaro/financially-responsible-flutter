class WishlistIncome {
  final int? id;
  final int wishlistGroupID;
  final int categoryID;

  WishlistIncome({
    this.id,
    required this.wishlistGroupID,
    required this.categoryID,
  });

  factory WishlistIncome.fromMap(Map<String, dynamic> json) => WishlistIncome(
        id: json['id'],
        wishlistGroupID: json['wishlistGroupID'],
        categoryID: json['categoryID'],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) map['id'] = id;
    map['wishlistGroupID'] = wishlistGroupID;
    map['categoryID'] = categoryID;
    return map;
  }
}
