class ActivityItemTemplate {
  final int? id;
  final bool isIncome;
  final int categoryID;
  final String? description;

  ActivityItemTemplate({
    this.id,
    required this.isIncome,
    required this.categoryID,
    this.description,
  });

  factory ActivityItemTemplate.fromMap(Map<String, dynamic> json) =>
      ActivityItemTemplate(
        id: json['id'],
        isIncome: json['isIncome'] == 1,
        categoryID: int.parse(json['categoryID']),
        description: json['description'],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) map['id'] = id;
    map['isIncome'] = isIncome ? 1 : 0;
    map['categoryID'] = categoryID;
    if (description != null) map['description'] = description;
    return map;
  }
}
