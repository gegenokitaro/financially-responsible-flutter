class Wishlist {
  final int? id;
  final String name;
  final double price;
  final int? wishlistGroupID;

  Wishlist({
    this.id,
    required this.name,
    required this.price,
    required this.wishlistGroupID,
  });

  factory Wishlist.fromMap(Map<String, dynamic> json) => Wishlist(
        id: json['id'],
        name: json['name'],
        price: json['price'],
        wishlistGroupID: json['wishlistGroupID'],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) map['id'] = id;
    map['name'] = name;
    map['price'] = price;
    if (wishlistGroupID != null) map['wishlistGroupID'] = wishlistGroupID;
    return map;
  }
}
