class ActivityCategory {
  final int? id;
  final bool isIncome;
  final String name;
  final int walletID;
  final int? delegateCategoryID;

  ActivityCategory({
    this.id,
    required this.isIncome,
    required this.name,
    required this.walletID,
    this.delegateCategoryID,
  });

  factory ActivityCategory.fromMap(Map<String, dynamic> json) =>
      ActivityCategory(
        id: json['id'],
        isIncome: json['isIncome'] == 1 ? true : false,
        name: json['name'],
        walletID: json['walletID'],
        delegateCategoryID: json['delegateCategoryID'],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) map['id'] = id;
    map['isIncome'] = isIncome ? 1 : 0;
    map['name'] = name;
    map['walletID'] = walletID;
    if (delegateCategoryID != null) {
      map['delegateCategoryID'] = delegateCategoryID;
    }
    return map;
  }
}
