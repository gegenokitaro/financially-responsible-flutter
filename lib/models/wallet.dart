class Wallet {
  int? id;
  String name;

  Wallet({
    this.id,
    required this.name,
  });

  factory Wallet.fromMap(Map<String, dynamic> json) => Wallet(
        id: json['id'],
        name: json['name'],
      );

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    if (id != null) map['id'] = id;
    map['name'] = name;
    return map;
  }
}
