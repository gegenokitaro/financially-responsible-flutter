import 'package:flutter/material.dart';
import 'package:frflutter/helpers/crud/crud_item.dart';
import 'package:frflutter/helpers/crud/crud_wallet.dart';
import 'package:frflutter/models/activity_item.dart';
import 'package:frflutter/models/wallet.dart';
import 'package:frflutter/providers/current_wallet_provider.dart';
import 'package:frflutter/widgets/list_wallet.dart';
import 'package:provider/provider.dart';

class WalletPage extends StatefulWidget {
  const WalletPage({super.key});

  @override
  State<WalletPage> createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  _showWalletList(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (context) => const ListWallet(),
    );
  }

  _testData() async {}

  @override
  Widget build(BuildContext context) {
    print('wallet page');
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          title: Row(
            children: [
              Ink(
                padding: const EdgeInsets.all(0),
                child: InkWell(
                  onTap: () => _showWalletList(context),
                  child: Row(
                    children: [
                      const Icon(Icons.arrow_drop_down),
                      Consumer<CurrentWalletProvider>(
                        builder: (context, value, child) => FutureBuilder(
                            future: CRUDWallet.init.read(
                                where: 'id=?',
                                whereArgs: [value.currentWalletID]),
                            builder: (BuildContext context,
                                AsyncSnapshot<List<Wallet>> snapshot) {
                              if (!snapshot.hasData) {
                                return const Text(
                                  '...',
                                  style: TextStyle(fontSize: 16),
                                );
                              }
                              return Container(
                                padding: const EdgeInsets.only(
                                  top: 8,
                                  bottom: 8,
                                  right: 8,
                                ),
                                child: Text(
                                  snapshot.data!.first.name,
                                  style: const TextStyle(fontSize: 16),
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                width: 2,
              ),
              const Icon(
                Icons.edit,
                size: 14,
              )
            ],
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.all(4),
              child: GestureDetector(
                onTap: () {},
                child: const Icon(
                  Icons.filter_list_alt,
                  size: 16,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: GestureDetector(
                onTap: () {},
                child: const Icon(
                  Icons.more_vert,
                  size: 16,
                ),
              ),
            )
          ],
          flexibleSpace: const Row(
            children: [],
          ),
        ),
      ),
      body: FutureBuilder<List<ActivityItem>>(
        future: CRUDItem.init.read(),
        builder: (
          BuildContext context,
          AsyncSnapshot<List<ActivityItem>> snapshot,
        ) {
          if (!snapshot.hasData) return const Text('Loading...');
          return ListView(
            children: snapshot.data!
                .map((e) => Center(
                      child: Text(e.categoryName),
                    ))
                .toList(),
          );
        },
      ),
    );
  }
}
