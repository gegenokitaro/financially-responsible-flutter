import 'package:flutter/material.dart';
import 'package:frflutter/helpers/crud/crud_wallet.dart';

import '../models/wallet.dart';

class ListWallet extends StatefulWidget {
  const ListWallet({super.key});

  @override
  State<ListWallet> createState() => _ListWalletState();
}

class _ListWalletState extends State<ListWallet> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.2,
      constraints: const BoxConstraints(minHeight: 140),
      child: Column(
        children: [
          Expanded(
            child: FutureBuilder<List<Wallet>>(
              future: CRUDWallet.init.read(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: Text(
                      'Loading...',
                    ),
                  );
                }
                return ListView(
                  children: snapshot.data!
                      .map((e) => Ink(
                            padding: const EdgeInsets.all(0),
                            child: InkWell(
                              onTap: () {},
                              child: ListTile(
                                title: Text(e.name),
                              ),
                            ),
                          ))
                      .toList(),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
