import 'package:flutter/material.dart';

class ModalButton extends StatelessWidget {
  const ModalButton(
      {super.key,
      required this.backgroundColor,
      required this.foregroundColor,
      required this.text,
      required this.onPressed});

  final Color backgroundColor;
  final Color foregroundColor;
  final String text;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: backgroundColor,
                foregroundColor: foregroundColor,
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                side: BorderSide(color: Colors.blue.shade800),
                padding: const EdgeInsets.symmetric(vertical: 14)),
            onPressed: () => onPressed(context),
            child: Text(text)));
  }
}
