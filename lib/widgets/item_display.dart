import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/activity_item.dart';

class ItemDisplay extends StatelessWidget {
  const ItemDisplay({
    super.key,
    required this.item,
  });

  final ActivityItem item;

  @override
  Widget build(BuildContext context) {
    return Ink(
      child: InkWell(
        onTap: () {},
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 4),
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(width: 0.1, color: Colors.grey.shade500))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 60,
                child: Text(
                  item.categoryName,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              const SizedBox(
                width: 4,
              ),
              Expanded(
                  child: Text(
                "${item.description}",
                // overflow: TextOverflow.ellipsis,
              )),
              Container(
                alignment: Alignment.centerRight,
                width: 100,
                child: Text(
                  NumberFormat.currency(
                          locale: 'id', symbol: '', decimalDigits: 0)
                      .format(item.price),
                  style: TextStyle(
                      color: item.isIncome
                          ? Colors.blue.shade600
                          : Colors.red.shade600),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
