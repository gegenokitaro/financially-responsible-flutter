import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ItemInputClickable extends StatelessWidget {
  ItemInputClickable(
      {super.key,
      required this.iconLogo,
      required this.placeholder,
      required this.onTap,
      this.value});

  final IconData iconLogo;
  String placeholder;
  final Function onTap;
  String? value;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Ink(
        decoration: BoxDecoration(
            color: Colors.grey.shade200,
            borderRadius: BorderRadius.circular(6)),
        child: InkWell(
          borderRadius: BorderRadius.circular(6),
          onTap: () => onTap(context),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: Row(
              children: [
                Icon(iconLogo),
                const SizedBox(
                  width: 10,
                ),
                Text(value ?? placeholder)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
