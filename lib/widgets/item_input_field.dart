import 'package:flutter/material.dart';

class ItemInputField extends StatelessWidget {
  const ItemInputField(
      {super.key,
      required this.placeholder,
      required this.maxLines,
      required this.icon,
      this.inputType = TextInputType.multiline,
      required this.inputController});

  final String placeholder;
  final int maxLines;
  final IconData icon;
  final TextInputType inputType;
  final TextEditingController inputController;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          color: Colors.grey.shade200, borderRadius: BorderRadius.circular(6)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Icon(icon),
          ),
          const SizedBox(
            width: 10,
          ),
          Flexible(
            child: TextField(
              controller: inputController,
              keyboardType: inputType,
              cursorColor: Colors.grey,
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black87,
              ),
              decoration: InputDecoration(
                  isDense: true,
                  contentPadding:
                      const EdgeInsets.symmetric(horizontal: 0, vertical: 12),
                  hintText: placeholder,
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none),
              maxLines: maxLines,
            ),
          ),
        ],
      ),
    );
  }
}
