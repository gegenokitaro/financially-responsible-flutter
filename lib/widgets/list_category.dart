import 'dart:async';

import 'package:flutter/material.dart';

import '../models/activity_category.dart';

class ListCategory extends StatefulWidget {
  const ListCategory({
    super.key,
    required this.isIncome,
  });

  final bool isIncome;

  @override
  State<ListCategory> createState() => _ListCategoryState();
}

class _ListCategoryState extends State<ListCategory> {
  Timer? _debounce;
  final _searchController = TextEditingController();
  List<ActivityCategory> filteredCategories = [];

  @override
  void dispose() {
    // TODO: implement dispose
    _debounce?.cancel();
    _searchController.dispose();
    super.dispose();
  }

  void onSearchChanged(String query, List<ActivityCategory> categories) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      var list = categories.where((e) => e.name.contains(query)).toList();
      setState(() {
        filteredCategories = list;
      });
      print(filteredCategories.length);
    });
  }

  addCategory() {
    if (_searchController.text.isEmpty) return;
    _searchController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height * 0.9,
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            height: 50,
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    onChanged: (query) =>
                        onSearchChanged(query, filteredCategories),
                    controller: _searchController,
                  ),
                ),
                Visibility(
                  visible: filteredCategories.isEmpty,
                  child: ElevatedButton(
                      onPressed: addCategory,
                      child: const Text("Tambah Kategori")),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
                key: UniqueKey(),
                itemCount: filteredCategories.length,
                itemBuilder: (_, index) {
                  final ActivityCategory category = filteredCategories[index];
                  return SizedBox(
                    width: double.infinity,
                    child: Text(category.name),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
