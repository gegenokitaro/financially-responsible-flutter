import 'package:flutter/foundation.dart';
import 'package:frflutter/models/activity_item.dart';

class ActivityItemsProvider with ChangeNotifier {
  List<ActivityItem> _activityItems = <ActivityItem>[];
  List<ActivityItem> get activityItems => _activityItems;

  update(List<ActivityItem> activityItems) {
    _activityItems = activityItems;
    notifyListeners();
  }

  add(ActivityItem item) {
    _activityItems = [...activityItems, item];
    notifyListeners();
  }
}
