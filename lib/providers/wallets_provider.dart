import 'package:flutter/foundation.dart';
import 'package:frflutter/models/wallet.dart';

class WalletsProvider with ChangeNotifier {
  List<Wallet> _wallets = <Wallet>[];
  List<Wallet> get wallets => _wallets;

  update(List<Wallet> listWallets) {
    _wallets = listWallets;
    notifyListeners();
  }

  add(Wallet wallet) {
    _wallets = [...wallets, wallet];
    notifyListeners();
  }
}
