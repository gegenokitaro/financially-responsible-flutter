import 'package:flutter/foundation.dart';
import 'package:frflutter/models/activity_category.dart';

class ActivityCategoriesProvider with ChangeNotifier {
  List<ActivityCategory> _activityCategories = <ActivityCategory>[];
  List<ActivityCategory> get activityCategories => _activityCategories;

  update(List<ActivityCategory> activityCategories) {
    _activityCategories = activityCategories;
    notifyListeners();
  }

  add(ActivityCategory category) {
    _activityCategories.add(category);
    notifyListeners();
  }
}
