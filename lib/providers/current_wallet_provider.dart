import 'package:flutter/foundation.dart';

class CurrentWalletProvider with ChangeNotifier {
  int _currentWalletID = 1;
  int get currentWalletID => _currentWalletID;

  update(int walletID) {
    _currentWalletID = walletID;
    notifyListeners();
  }
}
