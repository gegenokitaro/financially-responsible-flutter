## Financially Responsible (with Flutter)

### Description

This is my yet another project that share the same name with my previous one, because I'm done with react native, especially expo. It just doesn't work for me. Expo Go suddenly can't preview my app (using real phone), no longer provide local build (have to submit the code to their service and built them there, and have to queue for a free user), and many more inconveniences that made me looks like I just want to rant here for no reason if I stated all of them.

So, I decided to rewrite this app using Flutter. There're at least 3 reasons why I moved to Flutter:

1. I can build locally.
2. I can preview my app on my phone. Without inconveniences.
3. Sooner or later, I have to work with FLutter anyway for my current day job. It's just a learning process for me.

### Todo

Since this project is for my personal usage (and my wife), the features still the same. With some additional that I think I need.

- [ ] Basic Feature
  - [ ] Acitivity Item (Financial activity record)
    - [ ] CRUD
    - [ ] Display Activity
    - [ ] Modal (Create, Update)
  - [ ] Category
    - [ ] CRUD
- [x] Implement SQLite
- [ ] Pages
  - [x] Navigation with floating action button
  - [ ] Main page (activity)
  - [ ] Second page (summary)
  - [ ] Third page (wishlist, new feature)
- [ ] Switchable database (new feature, for keeping different account)
